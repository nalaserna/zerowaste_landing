import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../model/Usuario';
import { DonadorService } from '../services/donador.service';
import { AES, enc, mode } from 'crypto-js';
import { LowerCasePipe } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  donadorRegistro: Usuario;
  donadorinicio: Usuario;


  constructor(private router: Router, private donadorService: DonadorService) {
    this.donadorRegistro = new Usuario();

  }

  ngOnInit() {
  }

  registrar() {
    this.router.navigate(['registro']);
  }

  encriptarclave(): string {

    // Encrypt
    const ciphertext = AES.encrypt(JSON.stringify(this.donadorRegistro.password), 'Alohomora');

    console.log(ciphertext.toString());

    // Decrypt

    return ciphertext;
  }


  desencriptar(ciphertext): string {


    const bytes = AES.decrypt(ciphertext.toString(), 'Alohomora');
    const decryptedData = JSON.parse(bytes.toString(enc.Utf8));

    console.log(decryptedData);
    return decryptedData;
  }

  inicioDonador() {

    this.donadorService.login(this.donadorRegistro.correo).subscribe(resultado => {
      this.donadorinicio = resultado;

      if (this.desencriptar(resultado.password) === this.donadorRegistro.password) {
        console.log('Donador inicio sesión' + resultado);
        window.localStorage.setItem('idUsuario', this.donadorinicio.id.toString());
        window.localStorage.setItem('rol', this.donadorinicio.tipoUsuario);

console.log(window.localStorage.getItem('rol'));



        window.location.href = 'http://localhost:4200/dashboard/' + this.donadorinicio.id + '/'+ this.donadorinicio.tipoUsuario;

      } else {
        alert('Usuario / contraseña incorrectos' );
        location.reload();
      }
    });

  }
}
