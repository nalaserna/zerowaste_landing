import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Recogida } from '../model/Recogida';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SolicitudService {

  constructor(private http: HttpClient) { }

  public getAllSolicitudes(): Observable<Recogida[]> {
    return this.http.get<Recogida[]>(environment.urlConsultarAllRecogidas);
  }

}
