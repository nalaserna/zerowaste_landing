import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Usuario } from '../model/Usuario';

@Injectable({
  providedIn: 'root'
})
export class DonadorService {

  constructor(private http: HttpClient) { }


  public addNewDonador(nombre: string, apellido: string, email: string, celular: string,
    cedula: string, password: string, fecha_nacimiento: string): Observable<string> {

    const parametros = new HttpParams()
      .set('nombre', nombre + '')
      .set('apellido', apellido + '')
      .set('email', email + '')
      .set('celular', celular + '')
      .set('cedula', cedula + '')
      .set('password', password + '')
      .set('fecha_nacimiento', fecha_nacimiento + '');
    const httpParams = parametros;

    return this.http.post<string>(environment.urladdNewDonador, httpParams);
  }

  public login(email: string): Observable<Usuario> {
    const body = new HttpParams().set('email', email + '');
    return this.http.post<Usuario>(environment.urlgetDonadorByEmail, body);
  }


}
