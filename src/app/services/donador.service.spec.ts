import { TestBed } from '@angular/core/testing';

import { DonadorService } from './donador.service';

describe('DonadorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DonadorService = TestBed.get(DonadorService);
    expect(service).toBeTruthy();
  });
});
