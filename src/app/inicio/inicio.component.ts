import { Component, OnInit } from '@angular/core';
import { SolicitudService } from '../services/solicitud.service';
import { Router } from '@angular/router';
import { Recogida } from '../model/Recogida';
import { Usuario } from '../model/Usuario';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  misRecogidas: Array<Recogida>;
  miRecogida: Recogida;
  user: Usuario;

  constructor() {
  // constructor(private solicitudService: SolicitudService, private router: Router) {
    // this.miRecogida = new Recogida();
    // this.user = new Usuario();
    // solicitudService.getAllSolicitudes().subscribe(resp => {
    //   this.misRecogidas = resp;
    //   console.log('Solicitudes consultadas');
    // });
  }

  ngOnInit() {
  }

}
