import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../model/Usuario';
import { DonadorService } from '../services/donador.service';
import { AES, enc, mode } from 'crypto-js';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  donadorRegistro: Usuario;

  constructor(private router: Router, private donadorService: DonadorService) {
    this.donadorRegistro = new Usuario();
  }

  ngOnInit() {
  }

  inicio() {
    this.router.navigate(['login']);
  }

  encriptarclave(): string {

    // Encrypt
    const ciphertext = AES.encrypt(JSON.stringify(this.donadorRegistro.password), 'Alohomora');

    // console.log(ciphertext.toString());

    // // Decrypt
    // const bytes = AES.decrypt(ciphertext.toString(), 'Alohomora');
    // const decryptedData = JSON.parse(bytes.toString(enc.Utf8));

    // console.log(decryptedData);
    return ciphertext;
  }



  crearDonador() {
    console.log('Solicitud:', this.encriptarclave());

    this.donadorService.addNewDonador(this.donadorRegistro.nombre, this.donadorRegistro.apellido,
      this.donadorRegistro.correo, this.donadorRegistro.celular.toString(),
      this.donadorRegistro.cedula, this.encriptarclave(), this.donadorRegistro.fechaNacimiento.toString()).subscribe(resultado => {
        console.log('Donador registrado' + resultado);
      });
      this.router.navigate(['/login']);

  }


}
