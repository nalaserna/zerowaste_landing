import { Usuario } from './Usuario';

export class Recogida {

    public id: Number;
    public localidad: Number;
    public donador: Usuario;
    public recolector: Usuario;
    public fechaSolicitud: Date;
    public fechaEstimadaEntrega: Date;
    public fechaCompletada: Date;
    public direccion: string;
}
